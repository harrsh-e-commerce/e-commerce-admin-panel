const routes = {
    public: {
        signin: '/signin',
        signup: '/signup',
        forgotPassword: '/forgot-password',
    },
    private: {
        dashboard: '/dashboard',
    },
};

export default routes;
